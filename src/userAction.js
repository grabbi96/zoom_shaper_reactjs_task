
export const fetchUserProfile = (id) => {
 let promise =  fetch(
    `https://jsonplaceholder.typicode.com/users/${id}`
  ).then((response) => response.json())

  let status = 'pending';
  let result;
  let suspender = promise.then(
    (r) => {
      status = 'success';
      result = r;
    },
    (e) => {
      status = 'error';
      result = e;
    }
  );
  return {
    data() {
      if (status === 'pending') {
        throw suspender;
      } else if (status === 'error') {
        throw result;
      } else if (status === 'success') {
        return result;
      }
    },
  };

};


