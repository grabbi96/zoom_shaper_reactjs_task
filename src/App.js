import React, { Suspense } from 'react';

import { fetchUserProfile } from './userAction';

const SuspensefulUserProfile = ({  userId }) => {
  const userInfo = userId.data();
  return <UserProfile data={userInfo} />;
};

const UserProfile = ({ data }) => {
  return (
    <div className="info_wrapper">
      <h2>{data.name}</h2>
      <h4>{data.email}</h4>
    </div>
  );
};

const App = () => (
  <div className="App">
    <Suspense fallback={<p>Loading user...</p>}>
      <SuspensefulUserProfile userId={fetchUserProfile(8)}  />
      <SuspensefulUserProfile  userId={fetchUserProfile(9)} />
      <SuspensefulUserProfile  userId={fetchUserProfile(10)} />
    </Suspense>
  </div>
);

export default App;
